const mat2 = glMatrix.mat2;
const mat2d = glMatrix.mat2d;
const mat3 = glMatrix.mat3;
const mat4 = glMatrix.mat4;

const quat = glMatrix.quat;
const quat2 = glMatrix.quat2;

const vec2 = glMatrix.vec2;
const vec3 = glMatrix.vec3;
const vec4 = glMatrix.vec4;

const canvas = document.getElementById('glCanvas');
/** @type {WebGL2RenderingContext} */
const gl = canvas.getContext('webgl2');

class Vertex{
    constructor(position, normal){
        this._position = position;
        this._normal = normal;
    }

    get position(){
        return this._position;
    }

    get normal(){
        return this._normal;
    }
}

class Mesh{
    _vao;
    _vaoLines;
    _vbo;
    _indexBuffer;
    _lineBuffer;

    _indices;
    _lineIndices;
    

    _solidColor;
    _lineColor;
    _transformation;

    constructor(solidColor, lineColor){
        this._solidColor = solidColor;
        this._lineColor = lineColor;
        this._transformation = mat4.create();
    }

    get transformation(){
        return this._transformation;
    }

    translate(direction){
        mat4.translate(this._transformation, this._transformation, direction);
    }

    resetTransform(){
        this._transformation = mat4.create();
    }

    rotate(rotation){
        let quaternion = quat.create();
        let rot = mat4.create();
        quat.fromEuler(quaternion, rotation[0], rotation[1], rotation[2]);
        mat4.fromQuat(rot, quaternion);
        mat4.multiply(this._transformation, this._transformation, rot);
    }

    scale(scale){
        mat4.scale(this._transformation, this._transformation, scale);
    }
    
    uniformScale(uniformScale){
       this.scale([uniformScale, uniformScale, uniformScale]);
    }

    transform(scale, rotation, translation){
        this.scale(scale);
        this.rotate(rotation);
        this.translate(translation);
    }

    setPosition(position){
        let translation = mat4.create();
        translation[12] = position[0];
        translation[13] = position[1];
        translation[14] = position[2];
        mat4.multiply(this._transformation, this._transformation, translation);
    }

    setupMesh(vertices, indices, lineIndices){
        
        this._indices = indices;
        this._lineIndices = lineIndices;

        this._vao = gl.createVertexArray();
        this._vaoLines = gl.createVertexArray();
        this._vbo = gl.createBuffer();
        this._indexBuffer = gl.createBuffer();
        this._lineBuffer = gl.createBuffer();


        // 3 x 4bytes for position, 4 x 4bytes for normal
        const bytesPerVertex = 28;

        const buffer = new ArrayBuffer(bytesPerVertex * vertices.length);
        const dataView = new DataView(buffer);

        for(let i = 0; i < this._vertices.length; i++){
            dataView.setFloat32(bytesPerVertex * i,vertices[i].position[0], true);
            dataView.setFloat32(bytesPerVertex * i + 4, vertices[i].position[1], true);
            dataView.setFloat32(bytesPerVertex * i + 8, vertices[i].position[2], true);


            dataView.setFloat32(bytesPerVertex * i + 12, vertices[i].normal[0], true);
            dataView.setFloat32(bytesPerVertex * i + 16, vertices[i].normal[1], true);
            dataView.setFloat32(bytesPerVertex * i + 20, vertices[i].normal[2], true);
            dataView.setFloat32(bytesPerVertex * i + 24, vertices[i].normal[3], true);

        }

        // Solid
        gl.bindVertexArray(this._vao);
 
        gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);
        gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW);
        
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._indexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this._indices, gl.STATIC_DRAW);

        // vertex positions
        gl.vertexAttribPointer(0, 3, gl.FLOAT, false, bytesPerVertex, 0);
        gl.enableVertexAttribArray(0);
        
        // vertex normals
        gl.enableVertexAttribArray(1);
        gl.vertexAttribPointer(1, 4, gl.FLOAT, false, bytesPerVertex, 12);
        

        gl.bindVertexArray(null);
        
        // Wireframe
        gl.bindVertexArray(this._vaoLines);

        gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);
        gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._lineBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this._lineIndices, gl.STATIC_DRAW);

        // vertex positions
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0, 3, gl.FLOAT, false, bytesPerVertex, 0);

        // vertex normals
        gl.enableVertexAttribArray(1);
        gl.vertexAttribPointer(1, 4, gl.FLOAT, false, bytesPerVertex, 12);

        gl.bindVertexArray(null);
    }

    draw(shaderProgram){
        const uColor = gl.getUniformLocation(shaderProgram, "color");
        gl.uniform4fv(uColor, this._solidColor);
        gl.bindVertexArray(this._vao);
        gl.drawElements(gl.TRIANGLES, this._indices.length, gl.UNSIGNED_SHORT, 0);
        gl.bindVertexArray(null);
    }

    drawWireframe(shaderProgram){
        gl.useProgram(shaderProgram);
        const uColor = gl.getUniformLocation(shaderProgram, "color");
        gl.uniform4fv(uColor, this._lineColor);
        gl.bindVertexArray(this._vaoLines);
        gl.drawElements(gl.LINES, this._lineIndices.length, gl.UNSIGNED_SHORT,0);
        gl.bindVertexArray(null);
    }
}

class Grid extends Mesh {
    
    constructor(resolution, solidColor, lineColor) {
        super(solidColor, lineColor);
        this._vertices = this.generateVertices(resolution);
        const indexDict = this.generateIndices(resolution);
        this.setupMesh(this._vertices, indexDict.indices, indexDict.lineIndices);
    }

    generateVertices(resolution) {
        let vertices = [];
        let pos_x, pos_y, pos_z;
        const step = 2 / resolution;

        for (var i = 0, x = 0, z = 0; i < resolution * resolution; i++, x++) {
            let position = [];
            let normal = [0.0, 0.0, 1.0, 1.0];
            if (x === resolution) {
                x = 0;
                z += 1;
            }

            pos_x = ((x + 0.5) * step - 1.0);
            pos_y = 0.0;
            pos_z = ((z + 0.5) * step - 1.0);

            position.push(pos_x);
            position.push(pos_y);
            position.push(pos_z);
            vertices.push(new Vertex(position, normal));

        }
        return vertices;
    }

    generateIndices(resolution) {
        let indices = [];
        let lineIndices = [];
        const vertexCount = this._vertices.length;
        for (var i = 0; i < vertexCount - 1; i++) {
            
            if ((i + 1) % resolution === 0) {
                lineIndices.push(i);
                lineIndices.push(i + resolution);
                continue;
            }

            if(i > vertexCount - resolution - 1){
                lineIndices.push(i);
                lineIndices.push(i+1);
                continue;
            }

            indices.push(i);
            indices.push(i + resolution);
            indices.push(i + resolution + 1);

            indices.push(i);
            indices.push(i + 1);
            indices.push(i + resolution + 1);

            lineIndices.push(i);
            lineIndices.push(i + 1);
            lineIndices.push(i);
            lineIndices.push(i + resolution);
        }

        return {
            indices : new Uint16Array(indices), 
            lineIndices : new Uint16Array(lineIndices)
        };
    }
}

class Cube extends Mesh {
    constructor(solidColor, lineColor) {
        super(solidColor, lineColor);
        this.vertexData = this.createVertexData();
        this._vertices = this.vertexData.vertices;
        this._indices = this.vertexData.indices;
        this._lineIndices = this.vertexData.lineIndices;
        this.setupMesh(this._vertices, this._indices, this._lineIndices);
    }

    createVertexData() {
        

        const positions = [
            // Front face
            -1.0, -1.0, 1.0,
            1.0, -1.0, 1.0,
            1.0, 1.0, 1.0,
            -1.0, 1.0, 1.0,

            // Back face
            -1.0, -1.0, -1.0,
            -1.0, 1.0, -1.0,
            1.0, 1.0, -1.0,
            1.0, -1.0, -1.0,

            // Top face
            -1.0, 1.0, -1.0,
            -1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
            1.0, 1.0, -1.0,

            // Bottom face
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, -1.0, 1.0,
            -1.0, -1.0, 1.0,

            // Right face
            1.0, -1.0, -1.0,
            1.0, 1.0, -1.0,
            1.0, 1.0, 1.0,
            1.0, -1.0, 1.0,

            // Left face
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0,
            -1.0, 1.0, 1.0,
            -1.0, 1.0, -1.0,
        ];
        let vertices = [];

        for (var i = 0; i < positions.length;) {
            let position = [];
            let normal = []
            position.push(positions[i]);
            position.push(positions[i + 1]);
            position.push(positions[i + 2]);

            if(i < 24){
                normal = [0,0,positions[i + 2]];
            }
            else if(i < 48){
                normal = [0, positions[i + 1], 0];
            }
            else if(i < 72){
                normal = [positions[i], 0,0];
            }

            vertices.push(new Vertex(position, normal));
            i += 3;
        }


        let indices = []
        for( var i = 0; i < 24;){
            indices.push(i);
            indices.push(i + 1);
            indices.push(i + 2);
            indices.push(i + 2);
            indices.push(i + 3);
            indices.push(i);
            i += 4;
        }

        let indicesTris = new Uint16Array(indices);

        let lineIndices = [];
        for(var i = 0; i < 48;){
            lineIndices.push(i);
            lineIndices.push(i+1);
            lineIndices.push(i+1);
            lineIndices.push(i+2);
            lineIndices.push(i+2);
            lineIndices.push(i+3);
            lineIndices.push(i+3);
            lineIndices.push(i);
            i += 4;
        }
        let indicesLines = new Uint16Array(lineIndices);

        return{
            vertices: vertices,
            indices: indicesTris,
            lineIndices: indicesLines
        };

    }
}

class Sphere extends Mesh {

    constructor(resolution, solidColor, lineColor) {
        super(solidColor, lineColor);
        this.vertexData = this.createVertexData(resolution);
        this._vertices = this.vertexData.vertices;
        this._indices = this.vertexData.indices;
        this._lineIndices = this.vertexData.lineIndices;
        this.setupMesh(this._vertices, this._indices, this._lineIndices);
    }

    createVertexData(resolution) {
        var n = resolution;
        var m = resolution;

        // Index data.
        var indicesLines = new Uint16Array(2 * 2 * n * m);
        var indicesTris = new Uint16Array(3 * 2 * n * m);

        var du = 2 * Math.PI / n;
        var dv = Math.PI / m;
        var r = 1;
        // Counter for entries in index array.
        var iLines = 0;
        var iTris = 0;

        let vertices = [];
        // Loop angle u.
        for (var i = 0, u = 0; i <= n; i++, u += du) {
            // Loop angle v.
            for (var j = 0, v = 0; j <= m; j++, v += dv) {

                let position =[];
                let normal = [];

                var iVertex = i * (m + 1) + j;

                var x = r * Math.sin(v) * Math.cos(u);
                var y = r * Math.sin(v) * Math.sin(u);
                var z = r * Math.cos(v);

                // Set vertex positions.
                position.push(x);
                position.push(y);
                position.push(z);

                // Calc and set normals.
                var vertexLength = Math.sqrt(x * x + y * y + z * z);
                normal.push(x / vertexLength);
                normal.push(y / vertexLength);
                normal.push(z / vertexLength);
                normal.push(1.0);
                
                // Set index.
                // Line on beam.
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - 1;
                    indicesLines[iLines++] = iVertex;
                }
                // Line on ring.
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - (m + 1);
                    indicesLines[iLines++] = iVertex;
                }

                // Set index.
                // Two Triangles.
                if (j > 0 && i > 0) {
                    indicesTris[iTris++] = iVertex;
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                    //
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1) - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                }

                vertices.push(new Vertex(position, normal));
            }
        }
        return {
            vertices: vertices,
            indices: indicesTris,
            lineIndices: indicesLines
        };
    }
}

class Torus extends Mesh {
    constructor(resolution_n, resolution_m, solidColor, lineColor) {
        super(solidColor, lineColor);
        this.vertexData = this.createVertexData(resolution_n, resolution_m);
        this._vertices = this.vertexData.vertices;
        this._indices = this.vertexData.indices;
        this._lineIndices = this.vertexData.lineIndices;
        this.setupMesh(this._vertices, this._indices, this._lineIndices);
    }

    createVertexData(resolution_n, resolution_m) {
        var n = resolution_n;
        var m = resolution_m;

        // Positions.
        this.positions = new Float32Array(3 * (n + 1) * (m + 1));
        var positions = this.positions;
        // Normals.
        this.normals = new Float32Array(3 * (n + 1) * (m + 1));
        var normals = this.normals;
        // Index data.
        this.indicesLines = new Uint16Array(2 * 2 * n * m);
        var indicesLines = this.indicesLines;
        this.indicesTris = new Uint16Array(3 * 2 * n * m);
        var indicesTris = this.indicesTris;

        var du = 2 * Math.PI / n;
        var dv = 2 * Math.PI / m;
        var r = 0.15;
        var R = 0.5;
        // Counter for entries in index array.
        var iLines = 0;
        var iTris = 0;

        let vertices = [];
        // Loop angle u.
        for (var i = 0, u = 0; i <= n; i++, u += du) {
            // Loop angle v.
            for (var j = 0, v = 0; j <= m; j++, v += dv) {

                var iVertex = i * (m + 1) + j;
                let position = [];
                let normal = [];

                var x = (R + r * Math.cos(u)) * Math.cos(v);
                var y = (R + r * Math.cos(u)) * Math.sin(v);
                var z = r * Math.sin(u);

                // Set vertex positions.
                positions[iVertex * 3] = x;
                positions[iVertex * 3 + 1] = y;
                positions[iVertex * 3 + 2] = z;

                position.push(x);
                position.push(y);
                position.push(z);

                // Calc and set normals.
                var nx = Math.cos(u) * Math.cos(v);
                var ny = Math.cos(u) * Math.sin(v);
                var nz = Math.sin(u);

                normal.push(nx);
                normal.push(ny);
                normal.push(nz);
                normal.push(1.0);
                normals[iVertex * 3] = nx;
                normals[iVertex * 3 + 1] = ny;
                normals[iVertex * 3 + 2] = nz;

                // Set index.
                // Line on beam.
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - 1;
                    indicesLines[iLines++] = iVertex;
                }
                // Line on ring.
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - (m + 1);
                    indicesLines[iLines++] = iVertex;
                }

                // Set index.
                // Two Triangles.
                if (j > 0 && i > 0) {
                    indicesTris[iTris++] = iVertex;
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                    //
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1) - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                }
                vertices.push(new Vertex(position, normal));
            }
        }
        return {
            vertices: vertices,
            indices: indicesTris,
            lineIndices: indicesLines
        };
    }
}

class Camera {
    #FOV;
    #zNear;
    #zFar;
    #position;
    #target;
    #aspectRatio;

    #projectionMatrix;
    #viewMatrix;

    constructor(resolution, FOV, zNear, zFar, position, target){
        this.#aspectRatio = resolution[0] / resolution[1];
        this.#FOV = FOV * Math.PI / 180.0;
        this.#zNear = zNear;
        this.#zFar = zFar;
        this.#position = position;
        this.#target = target;

        this.#projectionMatrix = mat4.create();
        mat4.perspective(this.#projectionMatrix, this.#FOV, this.#aspectRatio, this.#zNear, this.#zFar);

        this.#viewMatrix = mat4.create();
        mat4.translate(this.#viewMatrix, this.#viewMatrix,this.#position);
        mat4.targetTo(this.#viewMatrix, this.#position, this.#target, [0.0, 1.0, 0.0]);
    }

    get projection(){
        return this.#projectionMatrix;
    }

    get view(){
        return this.#viewMatrix;
    }

    get inverseView(){
        let inverse = mat4.create();
        mat4.invert(inverse, this.#viewMatrix);
        return inverse;
    }

    zoom(amount){
        this.move([0,0,-amount]);
        
    }

    moveAxis(direction){
        mat4.translate(this.#viewMatrix, this.#viewMatrix, direction);
    }

    move(direction){
        mat4.translate(this.#viewMatrix, this.#viewMatrix, direction);
        mat4.getTranslation(this.#position, this.#viewMatrix);
        mat4.targetTo(this.#viewMatrix, this.#position, this.#target, [0.0, 1.0, 0.0]);
    }

    moveAlongCircle(angle) {
        let angle_rad = angle * Math.PI / 180;
        let destination = vec3.create();
        vec3.rotateY(destination, this.#position, [0.0, 0.0, 0.0], angle_rad);
        
        let direction = vec3.create();
        vec3.sub(direction, destination, this.#position);
        
        this.#viewMatrix[12] += direction[0];
        this.#viewMatrix[13] += direction[1];
        this.#viewMatrix[14] += direction[2];
        mat4.getTranslation(this.#position, this.#viewMatrix);
        mat4.targetTo(this.#viewMatrix, this.#position, this.#target, [0.0, 1.0, 0.0]);
    }
}

class Scene{
    #camera;
    #meshes;
    
    constructor(){
        const resolution = [gl.canvas.clientWidth, gl.canvas.clientHeight];
        this.#camera = new Camera(resolution, 45, 0.1, 100, [0, 7, 12], [0, 2, 0]);
        this.#meshes = [];
        gl.enable(gl.DEPTH_TEST);
        gl.depthFunc(gl.LEQUAL);
    }

    get camera(){
        return this.#camera;
    }

    addMesh(mesh){
        this.#meshes.push(mesh);
    }

    drawSolid(solidShader){
        this.#clear();
        gl.useProgram(solidShader);
        const projMat = gl.getUniformLocation(solidShader, "uProjection");
        const viewMat = gl.getUniformLocation(solidShader, "uView");
        const modelTransform = gl.getUniformLocation(solidShader, "uWorld");
        gl.uniformMatrix4fv(projMat, false, this.#camera.projection);
        gl.uniformMatrix4fv(viewMat, false, this.#camera.inverseView);
        
        this.#meshes.forEach(mesh => {
            gl.uniformMatrix4fv(modelTransform, false, mesh.transformation);
            mesh.draw(solidShader);
        })
    }

    drawWireframe(wireframeShader){
        gl.useProgram(wireframeShader);
        const projMat = gl.getUniformLocation(wireframeShader, "uProjection");
        const viewMat = gl.getUniformLocation(wireframeShader, "uView");
        const modelTransform = gl.getUniformLocation(wireframeShader, "uWorld");
        gl.uniformMatrix4fv(projMat, false, this.#camera.projection);
        gl.uniformMatrix4fv(viewMat, false, this.#camera.inverseView);
        this.#meshes.forEach(mesh => {
            gl.uniformMatrix4fv(modelTransform, false, mesh.transformation);
            mesh.drawWireframe(wireframeShader);
            mesh.resetTransform();
        })
    }

    #clear(){
        gl.clearColor(1.0, 1.0, 1.0, 1.0);
        gl.clearDepth(1.0);
        gl.clear(gl.COLOR_BUFFER_BIT || gl.DEPTH_BUFFER_BIT);
    }
}


// Shader Loading Utils

function getShaderSource(url) {
    let req = new XMLHttpRequest();
    req.open("GET", url, false);
    req.send();
    return (req.status == 200) ? req.responseText : null;
};

function loadShader(gl, type, source) {
    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }
    return shader;
}

function initShaderProgram(gl, vsSource, fsSource) {
    const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
    const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
        return null;
    }

    return shaderProgram;
}

if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
}

const solidVertexShaderSource = `#version 300 es

layout (location = 0) in vec4 aVertexPosition;
layout (location = 1) in vec4 aNormal;

uniform mat4 uView;
uniform mat4 uProjection;
uniform mat4 uWorld;

out vec4 vertexColor;

void main() {
    gl_PointSize = 10.0;
    gl_Position = uProjection * uView * uWorld * aVertexPosition;
    vertexColor = 0.5 * aNormal;
}`;
const solidFragmentShaderSource = `#version 300 es
precision highp float;

in vec4 vertexColor;

out vec4 outColor;


void main(){
    outColor = vertexColor;
}`;

const wireframeVertexShaderSource = `#version 300 es

layout (location = 0) in vec4 aVertexPosition;
layout (location = 1) in vec4 aNormal;

uniform mat4 uView;
uniform mat4 uProjection;
uniform mat4 uWorld;


void main() {
    gl_PointSize = 10.0;
    gl_Position = uProjection * uView * uWorld * aVertexPosition;
}`;
const wireframeFragmentShaderSource = `#version 300 es
precision highp float;

out vec4 outColor;

uniform vec4 color;

void main(){
    outColor = color;
}`;

const depthVertexShaderSource = `#version 300 es

layout (location = 0) in vec4 aVertexPosition;
layout (location = 1) in vec4 aNormal;

uniform mat4 uView;
uniform mat4 uProjection;
uniform mat4 uWorld;

out vec4 vertexColor;

void main() {
    gl_PointSize = 10.0;
    gl_Position = uProjection * uView * uWorld * aVertexPosition;
    vertexColor = 0.5 * aNormal;
}`;
const depthFragmentShaderSource = `#version 300 es
precision highp float;

in vec4 vertexColor;

out vec4 outColor;


void main(){
    float depth = (1.0f - gl_FragCoord.w);
    outColor = vec4(depth, depth, depth, 1.0);
}`;

const solidShaderProgram = initShaderProgram(gl, solidVertexShaderSource, solidFragmentShaderSource);
const wireframeShaderProgram = initShaderProgram(gl, wireframeVertexShaderSource, wireframeFragmentShaderSource);
const depthShaderProgram = initShaderProgram(gl, depthVertexShaderSource, depthFragmentShaderSource);

let isDepthFrame = false;

const red = [1.0, 0.0, 0.0, 1.0];
const white = [1.0, 1.0, 1.0, 1.0];
const lightGrey = [0.8, 0.8, 0.8, 1.0];
const darkGrey = [0.4, 0.4, 0.4, 1.0];

const scene = new Scene();

const floor = new Grid(20, red, white);
scene.addMesh(floor);
floor.uniformScale(20);

const ring = new Torus(16, 32, lightGrey, darkGrey);
scene.addMesh(ring);
ring.setPosition([0, 0.8, 0]);
ring.uniformScale(8);
ring.rotate([-90, 0, 0]);

const cube1 = new Cube(lightGrey, darkGrey);
cube1.setPosition([4,2,0]);
cube1.scale([0.5, 2,0.5]);
scene.addMesh(cube1);

const ball1 = new Sphere(16, lightGrey, darkGrey);
scene.addMesh(ball1);
ball1.setPosition([4, 4, 0]);

const cube2 = new Cube(lightGrey, darkGrey);
cube2.setPosition([-4,2,0]);
cube2.scale([0.5, 2,0.5]);
scene.addMesh(cube2);

const ball2 = new Sphere(16, lightGrey, darkGrey);
scene.addMesh(ball2);
ball2.setPosition([-4, 4, 0]);

const cube3 = new Cube(lightGrey, darkGrey);
cube3.setPosition([0,2,4]);
cube3.scale([0.5, 2,0.5]);
scene.addMesh(cube3);

const ball3 = new Sphere(16, lightGrey, darkGrey);
scene.addMesh(ball3);
ball3.setPosition([0, 4, 4]);

const cube4 = new Cube(lightGrey, darkGrey);
cube4.setPosition([0,2,-4]);
cube4.scale([0.5, 2,0.5]);
scene.addMesh(cube4);

const ball4 = new Sphere(16, lightGrey, darkGrey);
scene.addMesh(ball4);
ball4.setPosition([0, 4, -4]);

const ring2 = new Torus(16, 32, lightGrey, darkGrey);
scene.addMesh(ring2);
ring2.setPosition([0, 5.4, 0]);
ring2.uniformScale(8);
ring2.rotate([-90, 0, 0]);

scene.drawSolid(depthShaderProgram);

function toggleMode(e){
    if(e.code === "Space"){
        isDepthFrame = !isDepthFrame;
    }

	if (isDepthFrame) {
		scene.drawSolid(depthShaderProgram);
	} else {
		scene.drawSolid(solidShaderProgram);
	}
}

function moveCamera(e) {
    switch (e.code) {
        case "KeyA":
            scene.camera.moveAlongCircle(-3);
            break;
        case "KeyD":
            scene.camera.moveAlongCircle(3);
            break;
        case "KeyW":
            scene.camera.move([0, 0.1, 0]);
            break;
        case "KeyS":
            scene.camera.move([0, -0.1, 0]);
            break;
        case "ArrowDown":
            scene.camera.moveAxis([0, -0.1, 0]);
            break;
        case "ArrowUp":
            scene.camera.moveAxis([0, 0.1, 0]);
            break;
        case "ArrowLeft":
            scene.camera.moveAxis([-0.1, 0, 0]);
            break;
        case "ArrowRight":
            scene.camera.moveAxis([0.1, 0, 0]);
            break;
        case "KeyI":
            scene.camera.zoom(0.1);
            break;
        case "KeyO":
            scene.camera.zoom(-0.1);
            break;
        default:
            break;
    }

	if (isDepthFrame) {
		scene.drawSolid(depthShaderProgram);
	} else {
		scene.drawSolid(solidShaderProgram);
	}
}

window.addEventListener('keydown', moveCamera);
window.addEventListener('keypress', toggleMode);
